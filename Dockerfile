from python:3.12-bookworm@sha256:23155f230e6f43ecb0670dc680feb3271d96b9ce99fe82b051bf7b668755e2ef as builder

ARG APP_VERSION=0.0.1

WORKDIR /build

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache


RUN --mount=type=cache,target=/root/.cache/pip pip install poetry --only=main

COPY pyproject.toml poetry.lock ./
RUN --mount=type=cache,target=$POETRY_CACHE_DIR poetry install

from python:3.12-slim-bookworm@sha256:e3ae8cf03c4f0abbfef13a8147478a7cd92798a94fa729a36a185d9106cbae32

ARG USERNAME=registry
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME
USER $USERNAME

WORKDIR /app

ENV VIRTUAL_ENV=/app/.venv
ENV PYTHONPATH="/app/.venv"
ENV PATH="/app/.venv/bin:$PATH"
ENV UVICORN_PORT=8000

COPY --from=builder --chown=$USER_UID:$USER_GID /build/.venv .venv
COPY --chown=$USER_UID:$USER_GID . .

EXPOSE 8000

ENTRYPOINT ["python", "-m", "uvicorn", "asyncapi_schema_registry.main:app", "--host", "0.0.0.0"]
