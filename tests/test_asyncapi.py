from asyncapi_schema_registry.asyncapi import Schema, lookup_ref
from assertpy import assert_that


class TestAsyncApi:
    def test_lookup_ref_message(self):
        schema = Schema("""
            info:
                version: 0.0.1
            components:
                messages:
                    amsg:
                        name: a msg
            """)

        r = lookup_ref("#/components/messages/amsg", schema=schema)
        assert_that(r["name"]).is_equal_to("a msg")
