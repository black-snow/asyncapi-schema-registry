import os
from pathlib import Path

from assertpy import assert_that

from asyncapi_schema_registry.asyncapi import Schema
from asyncapi_schema_registry.config import Config
from asyncapi_schema_registry.local_storage import Engine

SCHEMA_VERSION = "0.0.1"
SCHEMA_STR = f"info:\n  version: {SCHEMA_VERSION}\n"
SCHEMA: Schema = Schema(SCHEMA_STR)
SERVICE_NAME = "serv"


class TestLocalStorage:
    def test_store(self, tmp_path: Path):
        engine = _make_engine(tmp_path)
        expected_folder = tmp_path / SERVICE_NAME
        expected_file = expected_folder / f"{SERVICE_NAME}.{SCHEMA_VERSION}.yaml"

        engine.store(service=SERVICE_NAME, schema=SCHEMA)

        assert_that(os.path.isdir(expected_folder)).is_true()
        assert_that(os.path.isfile(expected_file)).is_true()
        with open(expected_file) as f:
            assert_that(f.read()).is_equal_to(SCHEMA_STR)

    def test_store_does_not_accept_slash(self, tmp_path: Path):
        engine = _make_engine(tmp_path)

        assert_that(engine.store).raises(ValueError).when_called_with(
            service="/etc/passwd", schema=SCHEMA
        )

    def test_store_does_not_accept_dot(self, tmp_path: Path):
        engine = _make_engine(tmp_path)

        assert_that(engine.store).raises(ValueError).when_called_with(
            service="..", schema=SCHEMA
        )

    def test_list(self, tmp_path: Path):
        engine = _make_engine(tmp_path)
        os.mkdir(Path(tmp_path) / SERVICE_NAME)
        os.mkdir(Path(tmp_path) / "anotherone")
        (tmp_path / "a.file").touch()

        services = engine.list_services()

        assert_that(services).contains(SERVICE_NAME).contains("anotherone")

    def test_list_service_versions(self, tmp_path: Path):
        engine = _make_engine(tmp_path)
        os.mkdir(Path(tmp_path) / SERVICE_NAME)
        (tmp_path / SERVICE_NAME / f"{SERVICE_NAME}.{SCHEMA_VERSION}.yaml").touch()
        (tmp_path / SERVICE_NAME / f"{SERVICE_NAME}.3.2.1.yaml").touch()

        versions = engine.list_service_versions(service=SERVICE_NAME)

        assert_that(versions).contains(SCHEMA_VERSION).contains("3.2.1")

    def test_list_service_versions_no_traversal(self, tmp_path: Path):
        engine = _make_engine(tmp_path)

        assert_that(engine.list_service_versions).raises(ValueError).when_called_with(
            service=".."
        )
        assert_that(engine.list_service_versions).raises(ValueError).when_called_with(
            service="/etc"
        )

    def test_get_service_version(self, tmp_path: Path):
        engine = _make_engine(tmp_path)
        os.mkdir(Path(tmp_path) / SERVICE_NAME)
        file = tmp_path / SERVICE_NAME / f"{SERVICE_NAME}.{SCHEMA_VERSION}.yaml"
        with open(file, "x") as f:
            f.write(SCHEMA_STR)

        schema = engine.get_service_version(service=SERVICE_NAME, version=SCHEMA_VERSION)
        assert_that(str(schema)).is_equal_to(SCHEMA_STR)


def _make_config(path: str) -> Config:
    c = Config()
    c.local_storage_path = path
    return c

def _make_engine(path: str) -> Engine:
    return Engine(_make_config(path))
