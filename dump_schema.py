#!/usr/bin/env python3

import yaml
import sys
from uvicorn.importer import import_from_string

if __name__ == "__main__":
    sys.path.insert(0, "asyncapi_schema_registry")
    app = import_from_string("asyncapi_schema_registry.main:app")
    openapi = app.openapi()
    version = openapi.get("openapi", "unknown version")
    
    with open("openapi.yaml", "w") as f:
        #json.dump(openapi, f, indent=2)
        yaml.dump(openapi, f, sort_keys=False)
