import logging
from typing import List, Dict

from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse, Response

from .asyncapi import Schema
from .config import Config, StorageBackend
from .storage_factory import StorageFactory
from .version import APP_VERSION

config = Config()
app = FastAPI(
    description="Opinionated AsycAPI + CloudEvents schema registry",
    version=APP_VERSION,
    license_info={
        "name": "MIT",
        "url": "https://opensource.org/license/mit",
    },
)
storage_factory = StorageFactory()
storage_engine = storage_factory.make(config.storage_backend, config)


@app.get("/health")
async def health() -> str:
    return "ok"

@app.get("/favicon.ico")
async def no_favicon():
    raise HTTPException(status_code=404)

@app.get("/config")
async def get_config() -> Dict:
    response = {
        "version": APP_VERSION,
        "storageBackend": config.storage_backend,
    }
    if config.storage_backend == StorageBackend.local:
        response["localStoragePath"] = config.local_storage_path
    return response


@app.get("/", tags=["service"])
async def list_services() -> List[str]:
    return storage_engine.list_services()


@app.post("/{service}", tags=["service"])
async def root_post(req: Request, service: str) -> Dict[str, str]:
    schema = Schema(await req.body())
    try:
        storage_engine.store(service, schema)
    except FileExistsError:
        logging.warn(f"file already exists {service} in version {schema.info.version}")
        raise HTTPException(status_code=422, detail="already exists")
    return {"service": service, "version": schema.info.version}


@app.get("/{service}/{version}", tags=["asyncapi"])
async def get_service_version(service: str, version: str, request: Request) -> Dict:
    accept_header = request.headers.get("accept")
    schema = storage_engine.get_service_version(service, version)
    if "application/x-yaml" in accept_header:
        return Response(
            content=str(schema),
            media_type="application/x-yaml",
        )
    elif "text/plain" in accept_header:
        return Response(
            content=str(schema),
            media_type="text/plain",
        )
    else:
        return JSONResponse(content=schema.get_yaml())


@app.get("/{service}", tags=["asyncapi"])
async def list_service_versions(service: str) -> List[str]:
    return storage_engine.list_service_versions(service)
