from asyncapi_schema_registry import config
from asyncapi_schema_registry.config import StorageBackend
from asyncapi_schema_registry import local_storage


class StorageFactory:
    def make(self, backend: StorageBackend, config: config) -> StorageBackend:
        if backend == StorageBackend.local:
            return local_storage.Engine(config)
        raise ValueError
