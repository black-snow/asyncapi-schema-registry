from typing import Dict, Self

from cloudevents.http import CloudEvent

# from cloudevents.conversion import from_dict
import yaml


class Info:
    version: str


class Components:
    messages: object
    schemas: object


class Schema:
    info: Info
    components: Components
    _yaml: object

    def __init__(self, schema_yaml: str):
        self._yaml = yaml.safe_load(schema_yaml)

        self.info = Info()
        self.info.version = self._yaml["info"]["version"]
        if "components" in self._yaml:
            self.components = Components
            if "messages" in self._yaml["components"]:
                self.components.messages = self._yaml["components"]["messages"]
            if "schemas" in self._yaml["components"]:
                self.components.messages = self._yaml["components"]["schemas"]

    def __str__(self) -> str:
        return yaml.dump(self._yaml)

    def get_yaml(self) -> object:
        return self._yaml

    def resolve_event(self, message) -> CloudEvent:
        '''extract CloudEvent from a message, resolving refs'''
        pass


def lookup_ref(ref: str, schema: Schema) -> Dict:
    _, _, *component = ref.split("/")
    if component[0] == "messages":
        return schema.components.messages[component[1]]
    if component[0] == "schemas":
        return schema.components.schemas[component[1]]
    raise ValueError
