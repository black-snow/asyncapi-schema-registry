from pydantic_settings import BaseSettings

# from pydantic import BaseModel, Field
from enum import Enum
from typing import Optional


class StorageBackend(str, Enum):
    local = "local"
    # azure_blob_storage = 'azure_blob_storage'
    # s3
    # gcs


class Config(BaseSettings):
    storage_backend: StorageBackend = StorageBackend.local
    local_storage_path: Optional[str] = "/tmp"
