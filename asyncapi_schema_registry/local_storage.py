import logging
import os
from pathlib import Path
from typing import List


import asyncapi_schema_registry.config as Config

from .asyncapi import Schema
from .storage import StorageBackend


class Engine(StorageBackend):
    def __init__(self, config: Config):
        self.config: Config = config
        self.path: Path = Path(self.config.local_storage_path)

    def store(self, service: str, schema: Schema) -> None:
        self._safe_path(service)

        try:
            logging.info(f"making {self.path / service}")
            os.mkdir(self.path / service)
        except FileExistsError:
            pass
        with open(
            self.path / service / f"{service}.{schema.info.version}.yaml", "x"
        ) as f:
            f.write(str(schema))

    def list_services(self) -> List[str]:
        dirs = [f.name for f in self.path.iterdir() if f.is_dir()]
        return dirs

    def list_service_versions(self, service: str) -> List[str]:
        self._safe_path(service)

        files = sorted(
            [
                ".".join(os.path.splitext(f.name)[0].split(".")[1:])
                for f in (self.path / service).iterdir()
                if f.is_file()
            ]
        )
        return files

    def get_service_version(self, service: str, version: str) -> Schema:
        self._safe_path(service)
        self._safe_version(version)

        with open(self.path / service / f"{service}.{version}.yaml", "r") as f:
            return Schema(f.read())

    def _safe_path(self, path: str):
        if "." in path or os.sep in path:
            logging.error(f"bad path {path}")
            raise ValueError

    def _safe_version(self, version: str):
        if os.sep in version:
            logging.error(f"bad version {version}")
            raise ValueError
