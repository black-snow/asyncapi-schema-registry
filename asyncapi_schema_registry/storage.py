from typing import List, Protocol
from .asyncapi import Schema


class StorageBackend(Protocol):
    def store(self, service: str, schema: Schema) -> None:
        pass

    def list_services(self) -> List[str]:
        pass

    def list_service_versions(self, service: str) -> List[str]:
        pass

    def get_service_version(self, service: str, version: str) -> Schema:
        pass
