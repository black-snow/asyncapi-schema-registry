# asyncapi-schema-registry

Basically a very opinionated AsyncAPI schema registry (v3 only) with pluggable storage backends and some extras:

* immutable versions
* component extraction (CloudEvents)

The code might be _shitty_ - I just need it to work, not to be pretty.

**How will it work?**

You upload a schema with a owning service name, let's call it `service`.
* We extract the `version`.
* Check & fail if `service:version` already exists in backend (or cache). TOCTOU would be neat but isn't that important.
* Extract messages, resolve references etc., check & fail if versioned types already exist
* Store everything

Now `service:version` schema should be available as well as `service/event/name:version`.  

## TODO

[x] basic setup  
[x] CR~UD~ basics  
[x] prod-ready OCI  
[] basic component extraction from components  
[] component extraction from messages directly  
[] resolving non-local refs
